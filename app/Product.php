<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [ 'name', 'price', 'description', 'count', 'status', 'image' ];
    protected $guardade = [ 'id', 'created_at', 'updated_at' ];
}
