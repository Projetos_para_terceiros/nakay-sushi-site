<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// Home
Route::get('/home', 'HomeController@index')->name('home');

// Products
Route::get('/products/ajaxDataTable', 'ProductController@ajaxDataTable');
Route::get('/products/ajaxNewImage', 'ProductController@ajaxNewImage');
Route::resource('/products', 'ProductController')->middleware('role:admin,vendedor');

// Units
Route::resource('/units', 'UnitController')->middleware('role:admin');