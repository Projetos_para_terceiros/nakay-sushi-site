<?php

use Faker\Generator as Faker;

$factory->define(App\Unit::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence(2),
        "street" => $faker->streetName,
        "neighborhood" => $faker->streetAddress,
        "number" => $faker->randomNumber(3),
        "cep" => $faker->randomNumber(8),
        "phone" => $faker->phoneNumber,
        "city_id" => 3106200
    ];
});
