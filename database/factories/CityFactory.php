<?php

use Faker\Generator as Faker;

$factory->define(App\City::class, function (Faker $faker) {
    return [
        'id' => $faker->randomNumber(),
        'name' => $faker->city,
        'state' => $str_random(2)
    ];
});
