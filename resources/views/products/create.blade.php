@extends('layouts.app')


@section('style')
<style>
    #im-product {
        width: 250px; 
        height: 250px;
        background-position: center center;
        background-repeat: no-repeat;
        background-image: url('http://via.placeholder.com/250');
    }

    .card-header {
        display: inline-flex;
        align-items:center;
    }

    .hidden {
        position: absolute;
        left: -99999px;
    }

    #in-image {
        display: block;
        cursor: pointer;
        background: grey;
        width: 250px;
        height: 250px;
        text-align: center;
        line-height: 30px;
        color: #fff;
    }

    #di-basic {
        display: inline-flex;
    }

    #di-basic-text {
        padding-left: 20px;
    }

    #la-inativo {
        padding-left: 10px;
    }

    #rb-status {
        width: auto;
        text-align: center;
    }

    #bt-save {
        float: right;
        width: 115px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Novo produto</h1></div>
                <div class="card-body">                    
                    {{ Form::open(['route' => 'products.store', 'files' => true]) }}
                    <div id="di-basic">
                        <div class="form-group">
                        <label for="in-image">
                            <img id="im-product" class="img-thumbnail" >                             
                        </label>
                            {{ Form::file('image', [ "class" => "hidden", "id" => "in-image" ] ) }}
                        </div>
                        <div id="di-basic-text">
                            <div class="form-group">
                                {{ Form::label('name', 'Nome') }}
                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('price', 'Preço') }}
                                {{ Form::number('price', null, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('count', 'Quantidade') }}
                                {{ Form::number('count', null, ['class' => 'form-control']) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Descrição') }}
                        {{ Form::textarea('description', null, ['class' => 'form-control']) }}
                    </div>
                    <div id="rb-status" class="form-group">
                        <label class="radio inline">
                            {{ Form::radio('status', 1, true, array('id'=>'ativo')) }}
                            Ativo
                        </label>    
                        <label id="la-inativo" class="radio inline">
                            {{ Form::radio('status', 0, false, array('id'=>'ativo')) }}
                            Inativo
                        </label>   
                    </div>
                    {{ Form::submit('Salvar', ['id' => 'bt-save', 'class' => 'upload btn btn-info']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
